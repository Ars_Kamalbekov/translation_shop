<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends Controller
{
    /**
     * @Route("/locale/{_locale}", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setLocaleAction(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/", name="homepage", requirements={"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('ruPhrase', TextType::class,['label' => 'Фраза на русском'])
            ->add('save', SubmitType::class, ['label' => 'Сохранить'])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $phrase = new Phrase();
            $phrase->translate('ru')->setDescription($data['ruPhrase']);

            $em = $this->getDoctrine()->getManager();
            $phrase->setUser($this->getUser());
            $em->persist($phrase);

            $phrase->mergeNewTranslations();
            $em->flush();

        }

        $phrases = $this->getDoctrine()->getRepository('AppBundle:Phrase')->findAll();

        return $this->render('main/index.html.twig', [
            'form' => $form->createView(),
            'phrases' => $phrases,
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("{details/{id}", requirements={"id": "\d+"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, $id)
    {
        $phrase = $this->getDoctrine()
                ->getRepository('AppBundle:Phrase')
                ->find($id);

        $locales = [
            'en' => null,
            'kg' => null,
            'fr' => null,
            'de' => null,
            'es' => null
        ];

        $formBuilder = $this->createFormBuilder();
        $translated = [];
        $translations  = $phrase->getTranslations();

        foreach ($locales as $key => $locale) {
            foreach ($translations as $translation) {
                if ($key == $translation->getLocale()) {
                    $translated[] = $translation->getLocale();
                    $locale[$key] = $translation->getDescription();
                }
            }

            if($locale == null){
                $formBuilder->add($key, TextType::class, ['required' => false]);
            }
        };

        $formBuilder->add('save', SubmitType::class,['label_format' => 'Сохранить переводы']);
        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $data_tr = array_keys($data);
            $key = 0;

            foreach ($data as $datum ){
                if($datum !== null){
                    $phrase->translate($data_tr[$key])->setDescription($datum);
                }
                $key++;
            }

            $em = $this->getDoctrine()->getManager();

            $phrase->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('app_base_details',array(
                'id' => $phrase->getId(),
        ));
        }

        return $this->render('main/details.html.twig', [
            'locales' => $translated,
            'user' => $this->getUser(),
            'phrase' => $phrase,
            'form' => $form->createView(),
        ]);
    }

}
