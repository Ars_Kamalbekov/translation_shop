<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Phrase;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;


class LoadArticleData implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user  = new User();
        $user
            ->setUsername('qwe')
            ->setEmail('qwe@qwe.qwe')
            ->setEnabled(true)
            ->setName('qwe');
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, 'qwe');
        $user->setPassword($password);

        $manager->persist($user);

        $phrase = new Phrase();
        $phrase
            ->setUser($user)
            ->translate('ru')->setDescription('Привет');

        $phrase->translate('en')->setDescription('Hello');
        $phrase->translate('kg')->setDescription('Salam');
        $phrase->translate('fr')->setDescription('Bonjour');

        $manager->persist($phrase);

        $phrase = new Phrase();
        $phrase->translate('ru')->setDescription('Как дела?');
        $phrase->translate('kg')->setDescription('Кандай?');

        $manager->persist($phrase);

        $manager->flush();
    }
}
